pub mod pipelines;

use bevy::prelude::*;

pub fn register(app: &mut App) {
    pipelines::register(app);
}
