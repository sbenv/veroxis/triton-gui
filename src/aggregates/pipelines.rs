use std::collections::BTreeMap;
use std::time::Duration;

use bevy::prelude::*;
use bevy::time::common_conditions::on_timer;

use crate::gitlab::gitlab_store::GitlabStore;

pub type PipelineId = u64;
pub type BuildId = u64;

#[derive(Debug, Default, Clone, Resource)]
pub struct PipelineAggregate {
    pub pipelines_preparing: u64,
    pub pipelines_failed: u64,
    pub pipelines_succeeded: u64,
    pub pipelines_canceled: u64,
    pub pipelines_skipped: u64,
    pub pipelines_running: u64,
    pub pipelines_total: u64,
    pub pipelines: BTreeMap<PipelineId, Pipeline>,
    pub builds_total: u64,
    pub builds_created: u64,
    pub builds_waiting_for_resource: u64,
    pub builds_pending: u64,
    pub builds_running: u64,
    pub builds_canceled: u64,
    pub builds_failed: u64,
    pub builds_success: u64,
    last_entry_timestamp: String,
    first_entry_timestamp: String,
}

#[derive(Debug, Default, Clone)]
pub struct Pipeline {
    pub pipeline_events: Vec<triton_gitlab::raw::events::pipeline::Pipeline>,
    pub builds: BTreeMap<BuildId, Vec<triton_gitlab::raw::events::build::Build>>,
}

pub fn register(app: &mut App) {
    app.add_systems(Startup, startup_system_create_pipeline_aggregate);
    app.add_systems(
        Update,
        update_pipeline_aggregate_system.run_if(on_timer(Duration::from_millis(250))),
    );
}

fn startup_system_create_pipeline_aggregate(mut commands: Commands) {
    commands.insert_resource(PipelineAggregate::default());
}

fn update_pipeline_aggregate_system(
    mut pipeline_aggregate_res: ResMut<PipelineAggregate>,
    gitlab_store: Res<GitlabStore>,
) {
    if *pipeline_aggregate_res.last_entry_timestamp == gitlab_store.last_entry_timestamp
        && *pipeline_aggregate_res.first_entry_timestamp == gitlab_store.first_entry_timestamp
    {
        return;
    }

    let mut pipeline_aggregate = PipelineAggregate::default();

    // collect pipelines and builds by pipeline id
    for pipeline in gitlab_store.pipelines.iter() {
        let p = pipeline_aggregate
            .pipelines
            .entry(pipeline.object_attributes.id)
            .or_default();
        p.pipeline_events.push(pipeline.clone());
    }
    for build in gitlab_store.builds.iter() {
        let p = pipeline_aggregate
            .pipelines
            .entry(build.pipeline_id)
            .or_default();
        let b = p.builds.entry(build.build_id).or_default();
        b.push(build.clone());
    }

    // pipelines: count summary
    let mut pipelines_preparing = 0;
    let mut pipelines_failed = 0;
    let mut pipelines_succeeded = 0;
    let mut pipelines_canceled = 0;
    let mut pipelines_skipped = 0;
    let mut pipelines_running = 0;
    for (_pipeline_id, pipeline) in pipeline_aggregate.pipelines.iter() {
        if let Some(pipeline_event) = pipeline.pipeline_events.last() {
            use triton_gitlab::raw::events::pipeline::PipelineStep::*;
            match &pipeline_event.object_attributes.status {
                Created => pipelines_preparing += 1,
                WaitingForResource => pipelines_preparing += 1,
                Pending => pipelines_preparing += 1,
                Running => pipelines_running += 1,
                Success => pipelines_succeeded += 1,
                Canceled => pipelines_canceled += 1,
                Failed => pipelines_failed += 1,
                Skipped => pipelines_skipped += 1,
                Unknown(_) => {}
            };
        }
    }
    pipeline_aggregate.pipelines_preparing = pipelines_preparing;
    pipeline_aggregate.pipelines_failed = pipelines_failed;
    pipeline_aggregate.pipelines_succeeded = pipelines_succeeded;
    pipeline_aggregate.pipelines_canceled = pipelines_canceled;
    pipeline_aggregate.pipelines_skipped = pipelines_skipped;
    pipeline_aggregate.pipelines_running = pipelines_running;
    pipeline_aggregate.pipelines_total = pipeline_aggregate.pipelines.len() as u64;
    pipeline_aggregate.last_entry_timestamp = gitlab_store.last_entry_timestamp.clone();
    pipeline_aggregate.first_entry_timestamp = gitlab_store.first_entry_timestamp.clone();

    let mut builds_total = 0;
    let mut builds_created = 0;
    let mut builds_waiting_for_resource = 0;
    let mut builds_pending = 0;
    let mut builds_running = 0;
    let mut builds_canceled = 0;
    let mut builds_failed = 0;
    let mut builds_success = 0;
    for (_pipeline_id, pipeline) in pipeline_aggregate.pipelines.iter() {
        for (_build_id, build_events) in pipeline.builds.iter() {
            builds_total += 1;
            if let Some(build) = build_events.last() {
                use triton_gitlab::raw::events::build::BuildStep::*;
                match &build.build_status {
                    Created => builds_created += 1,
                    WaitingForResource => builds_waiting_for_resource += 1,
                    Pending => builds_pending += 1,
                    Running => builds_running += 1,
                    Canceled => builds_canceled += 1,
                    Failed => builds_failed += 1,
                    Success => builds_success += 1,
                    Unknown(_) => {}
                }
            }
        }
    }
    pipeline_aggregate.builds_total += builds_total;
    pipeline_aggregate.builds_created = builds_created;
    pipeline_aggregate.builds_waiting_for_resource = builds_waiting_for_resource;
    pipeline_aggregate.builds_pending = builds_pending;
    pipeline_aggregate.builds_running = builds_running;
    pipeline_aggregate.builds_canceled = builds_canceled;
    pipeline_aggregate.builds_failed = builds_failed;
    pipeline_aggregate.builds_success = builds_success;

    *pipeline_aggregate_res = pipeline_aggregate;
}
