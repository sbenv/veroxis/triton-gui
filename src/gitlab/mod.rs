use std::time::Duration;

use bevy::prelude::*;
use bevy::time::common_conditions::on_timer;
use lazy_static::__Deref;

use self::gitlab_store::GitlabStore;
use self::triton_api::TritonApi;

pub mod gitlab_store;
pub mod triton_api;
mod triton_updater;

/// register all resources and systems for connecting to the `triton-gitlab-server` and updating the related bevy resources
pub fn register(app: &mut App) {
    app.add_systems(Startup, create_gitlab_store_resource);
    app.add_systems(Startup, create_triton_api_resource);
    app.add_systems(
        Update,
        triton_api_pre_update_system.run_if(on_timer(Duration::from_millis(250))),
    );
    app.add_systems(
        Update,
        shutdown_triton_api_system.after(triton_api_pre_update_system),
    );
}

fn create_triton_api_resource(mut commands: Commands) {
    commands.insert_resource(TritonApi::new());
}

fn create_gitlab_store_resource(mut commands: Commands) {
    commands.insert_resource(GitlabStore::default());
}

/// tries to read updates for the TritonApi
fn triton_api_pre_update_system(
    mut triton_api: ResMut<TritonApi>,
    mut gitlab_store: ResMut<GitlabStore>,
) {
    if !triton_api.is_connected() {
        if !gitlab_store.deref().received_data.is_empty() {
            *gitlab_store = GitlabStore::default();
        }
        return;
    }

    if let Some(new_gitlab_store) = triton_api.update() {
        *gitlab_store = new_gitlab_store;
    }
}

pub fn shutdown_triton_api_system(
    app_exit_event: EventReader<bevy::app::AppExit>,
    mut triton_api: ResMut<TritonApi>,
) {
    if !app_exit_event.is_empty() {
        info!("received event `AppExit`: shutting down");
        triton_api.cleanup();
        info!("shutdown finished");
    }
}
