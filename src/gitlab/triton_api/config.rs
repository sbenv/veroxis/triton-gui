use std::str::FromStr;

use anyhow::Result;
use chrono::NaiveDate;
use url::Url;

#[derive(Debug, Clone)]
pub struct TritonApiConfig {
    triton_url: String,
    triton_api_secret: Option<String>,
    pub min_date: NaiveDate,
    pub max_date: NaiveDate,
}

impl TritonApiConfig {
    pub fn new(
        triton_url: String,
        triton_api_secret: Option<String>,
        min_date: NaiveDate,
        max_date: NaiveDate,
    ) -> TritonApiConfig {
        Self {
            triton_url,
            triton_api_secret,
            min_date,
            max_date,
        }
    }

    pub fn get_url_query_events_for_date(&self, date: &NaiveDate) -> Result<String> {
        let mut triton_url = Url::from_str(self.triton_url.as_str())?;

        let triton_path = triton_url.path();
        let websocket_path = format!("{triton_path}api/webhook_events_all");
        triton_url.set_path(websocket_path.as_str());

        if let Some(secret) = self.triton_api_secret.as_ref() {
            triton_url.set_query(Some(format!("secret={secret}").as_str()));
        }

        let min_date = date.format("%Y-%m-%dT00:00:00Z").to_string();
        let max_date = date.format("%Y-%m-%dT23:59:59Z").to_string();
        let query = match triton_url.query() {
            Some(query) => format!("{query}&min_date={min_date}&max_date={max_date}"),
            None => format!("min_date={min_date}&max_date={max_date}"),
        };
        triton_url.set_query(Some(query.as_str()));

        let finalized_url = triton_url.to_string();

        Ok(finalized_url)
    }

    pub fn get_url_websocket(&self) -> Result<String> {
        let mut triton_url = Url::from_str(self.triton_url.as_str())?;

        // use `wss://` except if an explicit http url is given
        //
        // an encrypted connection should be the sensible default & fallback if no scheme is provided
        let websocket_scheme = match triton_url.scheme() {
            "http" => "ws",
            _ => "wss",
        };
        triton_url
            .set_scheme(websocket_scheme)
            .expect("real bad url given");

        let triton_path = triton_url.path();
        let websocket_path = format!("{triton_path}api/socket");
        triton_url.set_path(websocket_path.as_str());

        if let Some(secret) = self.triton_api_secret.as_ref() {
            triton_url.set_query(Some(format!("secret={secret}").as_str()));
        }

        Ok(triton_url.to_string())
    }
}
