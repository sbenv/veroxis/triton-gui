pub mod config;

use std::sync::mpsc::{Receiver, Sender};
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;

use bevy::prelude::*;
use chrono::{Days, Local, NaiveDate};
use scopeguard::defer;

use crate::gitlab::gitlab_store::GitlabStore;
use crate::gitlab::triton_api::config::TritonApiConfig;
use crate::gitlab::triton_updater;

/// An internal api which manages threading for the Gitlab Websocket, API Requests and the configuration for those.
///
/// The implementation within this module has to be scoped and isolated strictly.
#[derive(Debug, Clone, Resource)]
pub struct TritonApi {
    /// url to which the api was last connected
    triton_url: Option<String>,
    /// the api_secret if the last connection had an
    triton_api_secret: Option<String>,
    /// the min date which is send to the triton api
    triton_min_date: NaiveDate,
    /// the max date which is send to the triton api
    triton_max_date: NaiveDate,
    /// the max date which is send to the triton api if it is not `None`
    triton_is_connected: bool,
    /// the application is running if there is `Some` in the `Mutex`
    thread_critical_elements: Arc<Mutex<Option<ThreadCriticalElements>>>,
}

#[derive(Debug)]
struct ThreadCriticalElements {
    /// if active: contains a thread handle in which tokio is run
    triton_thread: Option<std::thread::JoinHandle<()>>,
    /// if active: contains a sender with which tokios thread may be closed
    triton_kill_tx: Sender<()>,
    /// if active: contains a receiver for GitlabStore updates
    gitlab_store_rx: Receiver<GitlabStore>,
    /// if active: used by the triton thread to communicate a failure in with its connection
    triton_conn_failed_rx: Receiver<()>,
}

impl TritonApi {
    pub fn new() -> TritonApi {
        TritonApi {
            triton_url: None,
            triton_api_secret: None,
            triton_min_date: Local::now()
                .naive_utc()
                .date()
                .checked_sub_days(Days::new(1))
                .unwrap(),
            triton_max_date: Local::now().naive_utc().date(),
            triton_is_connected: false,
            thread_critical_elements: Arc::new(Mutex::new(None)),
        }
    }

    /// BLOCKING
    pub fn connect(
        &mut self,
        triton_url: String,
        triton_api_secret: Option<String>,
        min_date: NaiveDate,
        max_date: NaiveDate,
    ) {
        let mut thread_critical_elements_lock = self.thread_critical_elements.lock().unwrap();
        if (*thread_critical_elements_lock).is_some() {
            warn!("could not connect because the api is still connected");
            return;
        }
        self.triton_url = Some(triton_url.clone());
        self.triton_api_secret = triton_api_secret.clone();
        self.triton_min_date = min_date;
        self.triton_max_date = max_date;
        self.triton_is_connected = true;
        let config = TritonApiConfig::new(triton_url, triton_api_secret, min_date, max_date);
        let (triton_kill_tx, triton_kill_rx) = std::sync::mpsc::channel::<()>();
        let (gitlab_store_tx, gitlab_store_rx) = std::sync::mpsc::channel::<GitlabStore>();
        let (triton_conn_failed_tx, triton_conn_failed_rx) = std::sync::mpsc::channel::<()>();
        let triton_thread = TritonApi::run_triton_thread(
            config,
            triton_kill_rx,
            gitlab_store_tx,
            triton_conn_failed_tx,
        );
        *thread_critical_elements_lock = Some(ThreadCriticalElements {
            triton_thread: Some(triton_thread),
            triton_kill_tx,
            gitlab_store_rx,
            triton_conn_failed_rx,
        });
    }

    // shuts down all background processes and resets the own state to default
    pub fn cleanup(&mut self) {
        self.triton_url = None;
        self.triton_api_secret = None;
        self.triton_min_date = Local::now()
            .naive_utc()
            .date()
            .checked_sub_days(Days::new(1))
            .unwrap();
        self.triton_max_date = Local::now().naive_utc().date();
        self.shutdown();
    }

    fn run_triton_thread(
        config: TritonApiConfig,
        kill_rx: Receiver<()>,
        store_tx: Sender<GitlabStore>,
        conn_failed_tx: Sender<()>,
    ) -> JoinHandle<()> {
        thread::spawn(|| {
            info!("triton thread was opened");
            defer!(info!("triton thread was closed"));
            use tokio::runtime;
            let rt = match runtime::Builder::new_multi_thread()
                .worker_threads(4)
                .enable_all()
                .build()
            {
                Ok(rt) => rt,
                Err(error) => {
                    error!("failed to create tokio runtime: {error}");
                    return;
                }
            };
            info!("tokio runtime was created");
            defer!(info!("tokio runtime was dropped"));
            rt.block_on(async {
                triton_updater::run(config, kill_rx, store_tx, conn_failed_tx).await;
            });
        })
    }

    /// shuts down all running background threads and tasks if there are any
    fn shutdown(&mut self) {
        let mut thread_critical_elements_lock = self.thread_critical_elements.lock().unwrap();
        if let Some(mut thread_critical_elements) = thread_critical_elements_lock.take() {
            // send the "please stop" signal
            if let Err(error) = thread_critical_elements.triton_kill_tx.send(()) {
                error!("TritonApi.shutdown(): {error}");
            }

            // wait for the thread to finish
            let join_thread = thread_critical_elements.triton_thread.take();
            if let Some(join_thread) = join_thread {
                if let Err(error) = join_thread.join() {
                    error!("TritonApi.shutdown(): {error:?}");
                }
            }
        }
        self.triton_is_connected = false;
    }

    pub fn triton_url(&self) -> Option<String> {
        self.triton_url.clone()
    }

    pub fn triton_min_date(&self) -> NaiveDate {
        self.triton_min_date
    }

    pub fn triton_max_date(&self) -> NaiveDate {
        self.triton_max_date
    }

    pub fn is_connected(&self) -> bool {
        self.triton_is_connected
    }

    /// BLOCKING
    pub fn update(&mut self) -> Option<GitlabStore> {
        let mut new_gitlab_store = None;
        let mut thread_critical_elements_lock = self.thread_critical_elements.lock().unwrap();
        let mut thread_critical_elements_owned = thread_critical_elements_lock.take();
        let mut close_requested = false;
        if let Some(thread_critical_elements) = thread_critical_elements_owned.as_mut() {
            match thread_critical_elements.triton_conn_failed_rx.try_recv() {
                Ok(_) => {
                    error!("received failed connection message - shutting down triton connection");
                    close_requested = true;
                }
                Err(error) => {
                    use std::sync::mpsc::TryRecvError::*;
                    match error {
                        Empty => loop {
                            match thread_critical_elements.gitlab_store_rx.try_recv() {
                                Ok(gitlab_store) => {
                                    new_gitlab_store = Some(gitlab_store);
                                }
                                Err(error) => match error {
                                    Empty => break,
                                    Disconnected => {
                                        error!("received disconnected message on `gitlab_store_rx` - shutting down triton connection");
                                        close_requested = true;
                                    }
                                },
                            }
                        },
                        Disconnected => {
                            error!("received disconnected message on `triton_conn_failed_rx` - shutting down triton connection");
                            close_requested = true;
                        }
                    }
                }
            }
        }
        *thread_critical_elements_lock = thread_critical_elements_owned;
        drop(thread_critical_elements_lock);
        if close_requested {
            self.cleanup();
            return None;
        }
        new_gitlab_store
    }

    /// BLOCKING
    pub fn set_min_date(&mut self, new_min_date: NaiveDate) {
        if self.is_connected() {
            if let Some(triton_url) = self.triton_url.as_ref() {
                let triton_url = triton_url.to_owned();
                self.shutdown();
                self.connect(
                    triton_url,
                    self.triton_api_secret.clone(),
                    new_min_date,
                    self.triton_max_date,
                );
            } else {
                error!("TritonApi says it is connected but does not have a url to which it is connected");
            }
        } else {
            self.triton_min_date = new_min_date;
        }
    }

    /// BLOCKING
    pub fn set_max_date(&mut self, new_max_date: NaiveDate) {
        if self.is_connected() {
            if let Some(triton_url) = self.triton_url.as_ref() {
                let triton_url = triton_url.to_owned();
                self.shutdown();
                self.connect(
                    triton_url,
                    self.triton_api_secret.clone(),
                    self.triton_min_date,
                    new_max_date,
                );
            } else {
                error!("TritonApi says it is connected but does not have a url to which it is connected");
            }
        } else {
            self.triton_max_date = new_max_date;
        }
    }
}
