use bevy::prelude::Resource;
use serde_json::Value;
use triton_gitlab::raw::events::build::Build;
use triton_gitlab::raw::events::deployment::Deployment;
use triton_gitlab::raw::events::feature_flag::FeatureFlag;
use triton_gitlab::raw::events::issue::Issue;
use triton_gitlab::raw::events::merge_request::MergeRequest;
use triton_gitlab::raw::events::note::Note;
use triton_gitlab::raw::events::pipeline::Pipeline;
use triton_gitlab::raw::events::push::Push;
use triton_gitlab::raw::events::release::Release;
use triton_gitlab::raw::events::wiki_page::WikiPage;

#[derive(Debug, Clone, Default, Resource)]
pub struct GitlabStore {
    pub first_entry_timestamp: String,
    pub last_entry_timestamp: String,
    pub received_data: Vec<Value>,
    pub builds: Vec<Build>,
    pub deployments: Vec<Deployment>,
    pub feature_flags: Vec<FeatureFlag>,
    pub issues: Vec<Issue>,
    pub merge_requests: Vec<MergeRequest>,
    pub notes: Vec<Note>,
    pub pipelines: Vec<Pipeline>,
    pub pushs: Vec<Push>,
    pub releases: Vec<Release>,
    pub wiki_pages: Vec<WikiPage>,
}

impl GitlabStore {
    pub fn sort_and_dedup(&mut self) {
        self.received_data.sort_by_key(|key| {
            key["triton_recv"]
                .as_str()
                .unwrap_or("0")
                .parse::<u128>()
                .ok()
                .unwrap_or(0)
        });
        self.received_data
            .dedup_by_key(|key| key["triton_recv"].clone());
        self.builds
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.builds.dedup_by_key(|val| val.triton_recv.clone());
        self.deployments
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.deployments.dedup_by_key(|val| val.triton_recv.clone());
        self.feature_flags
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.feature_flags
            .dedup_by_key(|val| val.triton_recv.clone());
        self.issues
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.issues.dedup_by_key(|val| val.triton_recv.clone());
        self.merge_requests
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.merge_requests
            .dedup_by_key(|val| val.triton_recv.clone());
        self.notes
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.notes.dedup_by_key(|val| val.triton_recv.clone());
        self.pipelines
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.pipelines.dedup_by_key(|val| val.triton_recv.clone());
        self.pushs
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.pushs.dedup_by_key(|val| val.triton_recv.clone());
        self.releases
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.releases.dedup_by_key(|val| val.triton_recv.clone());
        self.wiki_pages
            .sort_by_key(|val| val.triton_recv.parse::<u128>().ok().unwrap_or(0));
        self.wiki_pages.dedup_by_key(|val| val.triton_recv.clone());
    }
}
