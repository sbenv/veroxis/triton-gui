use anyhow::{bail, Result};
use bevy::prelude::*;
use futures_util::future;
use reqwest::{Response, StatusCode};
use serde_json::{json, Value};
use tokio::sync::broadcast;
use tokio::task::JoinHandle;

use crate::gitlab::triton_api::config::TritonApiConfig;

const PARALLEL_REQUEST_LIMIT: u8 = 4;

pub async fn fetch_existing_data(
    mut shutdown_rx: broadcast::Receiver<()>,
    config: TritonApiConfig,
) -> Result<Value> {
    let mut requests_collection = vec![];
    let mut values = vec![];

    let range = config.max_date - config.min_date;
    let amount_days_to_iterate = (range.num_days() as usize) + 1;

    let (tx, rx) = async_channel::unbounded::<()>();

    for date in config.min_date.iter_days().take(amount_days_to_iterate) {
        let triton_webhook_url = match config.get_url_query_events_for_date(&date) {
            Ok(url) => url,
            Err(error) => return Err(error),
        };

        let tx = tx.clone();
        let rx = rx.clone();
        let handle: JoinHandle<Result<Response>> = tokio::task::spawn(async move {
            let _ = rx.recv().await;
            info!("requesting data for [{}]", date);
            let r = reqwest::Client::builder()
                .build()
                .unwrap()
                .get(triton_webhook_url)
                .send()
                .await;
            info!("received response for [{}]", date);
            if let Err(err) = tx.send(()).await {
                error!("failed to notify next request: {err}");
            }
            match r {
                Ok(response) => Ok(response),
                Err(err) => Err(anyhow::format_err!("received error from triton: {err}")),
            }
        });
        requests_collection.push(handle);
    }

    for _ in 0..PARALLEL_REQUEST_LIMIT {
        if let Err(err) = tx.send(()).await {
            error!("failed to notify next request: {err}");
        }
    }

    tokio::select! {
        _ = shutdown_rx.recv() => {
            info!("initial_fetch: received shutdown signal");
            Ok(json!({}))
        },
        responses = future::join_all(requests_collection) => {
            for response in responses.into_iter() {
                let response = response?;
                let response = response?;
                if response.status() != StatusCode::OK {
                    bail!(
                        "received bad statuscode from triton: {}",
                        response.status()
                    );
                }
                let json_text = response.text().await?;
                let content_size = json_text.as_bytes().len();
                let human_size = human_bytes::human_bytes(content_size as f64);
                info!("received {human_size} ({content_size} bytes)");

                let values_for_date = serde_json::from_str::<Value>(&json_text)?;

                let values_for_date = match values_for_date {
                    Value::Array(values_for_date) => values_for_date,
                    unknown => {
                        error!("unexpected response from triton api: {unknown}");
                        continue;
                    }
                };
                for value in values_for_date {
                    values.push(value);
                }
            }

            Ok(json!(&values))
        },
    }
}
