use std::time::Duration;

use bevy::prelude::*;
use tokio::sync::broadcast;
use tokio::task::JoinHandle;
use tokio::time::sleep;

pub async fn session_manager(
    kill_rx: std::sync::mpsc::Receiver<()>,
) -> (broadcast::Receiver<()>, JoinHandle<()>) {
    let (shutdown_tx, shutdown_rx) = broadcast::channel::<()>(255);
    let handle = tokio::task::spawn(async move {
        loop {
            if kill_rx.try_recv().is_ok() {
                info!("received shutdown signal");
                if let Err(error) = shutdown_tx.send(()) {
                    error!("failed to broadcast shutdown signal: {error}");
                }
                break;
            }
            sleep(Duration::from_millis(10)).await;
        }
    });
    (shutdown_rx, handle)
}
