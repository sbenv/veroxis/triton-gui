mod existing_data_fetcher;
mod insert_data;
mod session_manager;
mod update_loop;
mod websocket;

use super::gitlab_store::GitlabStore;
use crate::gitlab::triton_api::config::TritonApiConfig;

pub async fn run(
    config: TritonApiConfig,
    kill_rx: std::sync::mpsc::Receiver<()>,
    gitlab_store_tx: std::sync::mpsc::Sender<GitlabStore>,
    conn_failed_tx: std::sync::mpsc::Sender<()>,
) {
    let (mut shutdown_rx, session_manager_handle) = session_manager::session_manager(kill_rx).await;
    let websocket_rx = shutdown_rx.resubscribe();
    let update_loop_rx = shutdown_rx.resubscribe();
    let initial_fetch_rx = shutdown_rx.resubscribe();
    if shutdown_rx.try_recv().is_ok() {
        bevy::log::error!("received kill signal before startup");
        return;
    }
    let mut gitlab_store = GitlabStore::default();
    let (websocket_rx, websocket_joinhandle) =
        match websocket::websocket(config.clone(), conn_failed_tx.clone(), websocket_rx) {
            Ok(ws) => ws,
            Err(error) => {
                bevy::log::error!("failed to connect websocket: {}", error);
                if let Err(error) = conn_failed_tx.send(()) {
                    bevy::log::error!("failed to send connection failure signal: {}", error);
                }
                return;
            }
        };
    let existing_data =
        match existing_data_fetcher::fetch_existing_data(initial_fetch_rx, config.clone()).await {
            Ok(existing_data) => existing_data,
            Err(error) => {
                bevy::log::error!("failed to query existing event data: {}", error);
                if let Err(error) = conn_failed_tx.send(()) {
                    bevy::log::error!("failed to send connection failure signal: {}", error);
                }
                return;
            }
        };
    if let Some(value_list) = existing_data.as_array() {
        for value in value_list.iter() {
            insert_data::insert_data(&mut gitlab_store, value);
        }
    }
    update_loop::run(
        update_loop_rx,
        gitlab_store,
        websocket_rx,
        gitlab_store_tx,
        conn_failed_tx,
    )
    .await;
    if let Err(error) = session_manager_handle.await {
        bevy::log::error!("session_manager connection failed to close: {}", error);
    }
    if let Err(error) = websocket_joinhandle.await {
        bevy::log::error!("websocket connection failed to close: {}", error);
    }
}
