use std::time::Duration;

use anyhow::Result;
use bevy::prelude::*;
use futures_util::{SinkExt, StreamExt};
use serde_json::Value;
use tokio::sync::{broadcast, mpsc};
use tokio::task::JoinHandle;
use tokio::time::sleep;
use tokio_tungstenite::connect_async_with_config;
use tokio_tungstenite::tungstenite::Message;
use triton_gitlab::parse_triton_recv::ParseTritonRecv;

use crate::gitlab::triton_api::config::TritonApiConfig;

pub fn websocket(
    config: TritonApiConfig,
    conn_failed_tx: std::sync::mpsc::Sender<()>,
    mut shutdown_rx: broadcast::Receiver<()>,
) -> Result<(tokio::sync::mpsc::Receiver<Value>, JoinHandle<()>)> {
    let (webhook_tx, webhook_rx) = mpsc::channel::<Value>(255);
    let triton_webhook_url = match config.get_url_websocket() {
        Ok(url) => url,
        Err(error) => return Err(error),
    };

    let handle = tokio::task::spawn(async move {
        info!("connecting to triton...");
        let (mut socket, _) =
            match connect_async_with_config(triton_webhook_url.as_str(), None, false).await {
                Ok(x) => x,
                Err(error) => {
                    error!("failed to connect to triton: {error}");
                    if let Err(error) = conn_failed_tx.send(()) {
                        error!("failed to notify TritonApi about connection failure: {error}");
                    }
                    return;
                }
            };
        info!("connecting to triton succeeded");
        loop {
            tokio::select! {
                Some(msg) = socket.next() => {
                    let message = match msg {
                        Ok(message) => message,
                        Err(error) => {
                            error!("error: {error}");
                            if let Err(error) = conn_failed_tx.send(()) {
                                error!("failed to notify TritonApi about connection close signal: {error}");
                            }
                            continue;
                        },
                    };
                    use Message::*;
                    match message {
                        Text(websocket_data) => {
                            let content_size = websocket_data.bytes().len() as u64;
                            let human_size = human_bytes::human_bytes(content_size as f64);
                            info!("received {human_size} ({content_size} bytes)");
                            match serde_json::from_str::<Value>(websocket_data.as_str()) {
                                Ok(websocket_data) => {
                                    if let Some(triton_recv) = websocket_data.parse_triton_recv() {
                                        let triton_recv = triton_recv.date_naive();
                                        if config.min_date > triton_recv {
                                            continue;
                                        }
                                        if config.max_date < triton_recv {
                                            continue;
                                        }
                                    }
                                    if let Err(error) = webhook_tx.send(websocket_data).await {
                                        error!("failed to send webhook: {error}");
                                    }
                                },
                                Err(error) => error!("failed to serialize received data: {error}"),
                            };
                        },
                        Binary(_) => {},
                        Ping(_) => {},
                        Pong(_) => {},
                        Close(signal) => {
                            warn!("received close signal: {signal:?}");
                            if let Err(error) = conn_failed_tx.send(()) {
                                error!("failed to notify TritonApi about connection close signal: {error}");
                            }
                        },
                        Frame(_) => {},
                    }
                },
                // waiting for a shutdown signal
                shutdown_signal = shutdown_rx.recv() => {
                    info!("received shutdown signal");
                    if let Err(error) = shutdown_signal {
                        error!("shutdown received error: {error}");
                    }
                    return;
                },
                // sending a ping every 2 seconds if nothing happens to keep connection alive
                _ = sleep(Duration::from_secs(2)) => {
                    if let Err(error) = socket.send(Message::Ping(vec![])).await {
                        error!("failed to send ping: {error}");
                    }
                }
            };
        }
    });
    Ok((webhook_rx, handle))
}
