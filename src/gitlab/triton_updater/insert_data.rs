use bevy::prelude::*;
use serde_json::Value;
use triton_gitlab::webhook::Webhook;

use crate::gitlab::gitlab_store::GitlabStore;

pub fn insert_data(gitlab_store: &mut GitlabStore, received_value: &Value) {
    // ugly but works:
    //
    // compare the incoming timestamp to the newset timestamp in our collection
    //
    // if it is older than what we have -> we've gotta sort things out after insertion
    let mut have_to_sort_and_dedup = false;
    if let Some(last_value) = gitlab_store.received_data.last() {
        if let Some(last_date_string) = last_value["triton_recv"].as_str() {
            if let Some(new_date_string) = received_value["triton_recv"].as_str() {
                if let Ok(last_date_timestamp) = last_date_string.parse::<u128>() {
                    if let Ok(new_date_timestamp) = new_date_string.parse::<u128>() {
                        if last_date_timestamp > new_date_timestamp {
                            have_to_sort_and_dedup = true;
                        }
                    }
                }
            }
        }
    }

    gitlab_store.received_data.push(received_value.clone());
    if let Some(last_value) = gitlab_store.received_data.last() {
        if let Some(timestamp) = last_value["triton_recv"].as_str() {
            gitlab_store.last_entry_timestamp = timestamp.to_owned();
        }
    }
    if let Some(first_value) = gitlab_store.received_data.first() {
        if let Some(timestamp) = first_value["triton_recv"].as_str() {
            gitlab_store.first_entry_timestamp = timestamp.to_owned();
        }
    }

    let webhook_parse_result = Webhook::try_from(received_value);
    match webhook_parse_result {
        Ok(webhook) => match &webhook {
            Webhook::Build(build) => {
                gitlab_store.builds.push(build.as_ref().to_owned());
            }
            Webhook::Deployment(deployment) => {
                gitlab_store
                    .deployments
                    .push(deployment.as_ref().to_owned());
            }
            Webhook::FeatureFlag(feature_flag) => {
                gitlab_store
                    .feature_flags
                    .push(feature_flag.as_ref().to_owned());
            }
            Webhook::Issue(issue) => {
                gitlab_store.issues.push(issue.as_ref().to_owned());
            }
            Webhook::MergeRequest(merge_request) => {
                gitlab_store
                    .merge_requests
                    .push(merge_request.as_ref().to_owned());
            }
            Webhook::Note(note) => {
                gitlab_store.notes.push(note.as_ref().to_owned());
            }
            Webhook::Pipeline(pipeline) => {
                gitlab_store.pipelines.push(pipeline.as_ref().to_owned());
            }
            Webhook::Push(push) => {
                gitlab_store.pushs.push(push.as_ref().to_owned());
            }
            Webhook::Release(release) => {
                gitlab_store.releases.push(release.as_ref().to_owned());
            }
            Webhook::TagPush(tag_push) => {
                gitlab_store.pushs.push(tag_push.as_ref().to_owned());
            }
            Webhook::WikiPage(wiki_page) => {
                gitlab_store.wiki_pages.push(wiki_page.as_ref().to_owned());
            }
        },
        Err(error) => {
            error!("{error:?}");
        }
    };

    if have_to_sort_and_dedup {
        warn!("detected a gitlab webhook being out of order - sorting now...");
        gitlab_store.sort_and_dedup();
        warn!("finished sorting");
    }
}
