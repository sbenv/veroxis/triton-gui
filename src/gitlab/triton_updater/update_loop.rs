use std::time::Duration;

use bevy::prelude::*;
use scopeguard::defer;
use serde_json::Value;
use tokio::sync::broadcast;
use tokio::time::sleep;

use crate::gitlab::gitlab_store::GitlabStore;
use crate::gitlab::triton_updater::insert_data::insert_data;

pub async fn run(
    mut shutdown_rx: broadcast::Receiver<()>,
    mut gitlab_store: GitlabStore,
    mut websocket_rx: tokio::sync::mpsc::Receiver<Value>,
    gitlab_store_tx: std::sync::mpsc::Sender<GitlabStore>,
    conn_failed_tx: std::sync::mpsc::Sender<()>,
) {
    // check if shutdown was already received
    if shutdown_rx.try_recv().is_ok() {
        info!("received shutdown signal");
        return;
    }
    // send the initial batch immediately
    if let Err(error) = gitlab_store_tx.send(gitlab_store.clone()) {
        error!("failed sending GitlabStore update: {error}");
    }
    info!("starting");
    defer!(info!("exiting"));
    loop {
        tokio::select! {
            // checking if the thread should shut down
            _ = shutdown_rx.recv() => {
                info!("received shutdown signal");
                return;
            }
            // waiting for websocket events to inject them into the gitlab_store
            received_data = websocket_rx.recv() => {
                match received_data {
                    Some(received_value) => {
                        insert_data(&mut gitlab_store, &received_value);
                        // send a new store after each received event
                        if let Err(error) = gitlab_store_tx.send(gitlab_store.clone()) {
                            error!("failed sending GitlabStore update: {error}");
                        }
                    }
                    None => {
                        error!("failed to receive data from websocket");
                        if let Err(error) = conn_failed_tx.send(()) {
                            bevy::log::error!("failed to send connection failure signal: {}", error);
                        }
                        sleep(Duration::from_millis(100)).await;
                    }
                }
            }
        }
    }
}
