use std::io::Write;
use std::path::PathBuf;

use anyhow::{bail, Result};
use bevy::prelude::*;
use serde::{Deserialize, Serialize};

const CONFIG_FILE_NAME: &str = "config.json";

/// register the configuration resource
pub fn register(app: &mut App) {
    app.add_systems(Startup, create_triton_gui_config_resource);
}

#[derive(Debug, Serialize, Deserialize, Clone, Default, Resource)]
pub struct TritonGuiConfig {
    pub active: Config,
    pub draft: Config,
}

/// global application configuration containing values stored in the config file
#[derive(Debug, Serialize, Deserialize, Clone, Default, Resource)]
pub struct Config {
    pub triton_instance_default: Option<String>,
    pub triton_instances: Vec<TritonInstanceConfig>,
}

/// create a single instance of the GitlabComponent
pub fn create_triton_gui_config_resource(mut commands: Commands) {
    let mut triton_gui_config = TritonGuiConfig::default();
    if let Ok(config) = Config::load() {
        triton_gui_config.active = config.clone();
        triton_gui_config.draft = config;
    }
    commands.insert_resource(triton_gui_config);
}

#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct TritonInstanceConfig {
    pub url: String,
    pub api_token: Option<String>,
}

impl Config {
    pub fn load() -> Result<Config> {
        let config_path = Config::get_config_path();
        match config_path.exists() {
            true => {
                let config_file_string = std::fs::read_to_string(config_path)?;
                match serde_json::from_str::<Config>(config_file_string.as_str()) {
                    Ok(config) => Ok(config),
                    Err(_) => {
                        warn!("failed to parse config: using default settings");
                        Ok(Config::default())
                    }
                }
            }
            false => {
                if let Some(config_dir) = config_path.parent() {
                    match std::fs::create_dir(config_dir) {
                        Ok(_) => match std::fs::File::create(config_path) {
                            Ok(mut file) => {
                                let triton_gui_config = Config::default();
                                match serde_json::to_string_pretty(&triton_gui_config) {
                                    Ok(json) => match file.write_all(json.as_bytes()) {
                                        Ok(_) => Ok(triton_gui_config),
                                        Err(error) => bail!(error),
                                    },
                                    Err(error) => bail!(error),
                                }
                            }
                            Err(error) => bail!(error),
                        },
                        Err(error) => bail!(error),
                    }
                } else {
                    unreachable!(
                        "the generated default config file path always has a parent directory"
                    )
                }
            }
        }
    }

    pub fn get_config_path() -> PathBuf {
        let project_dirs = directories::ProjectDirs::from("com", "Triton", "triton-gui").unwrap();
        let mut config_dir = project_dirs.config_dir().to_owned();
        config_dir.push(CONFIG_FILE_NAME);
        config_dir
    }

    #[allow(unused)]
    pub fn save(&self) -> Result<()> {
        use std::fs::File;
        use std::io::Write;

        let config_string = self.to_string();
        let config_path = Config::get_config_path();
        let mut config_file = File::create(config_path)?;
        writeln!(&mut config_file, "{config_string}",)?;
        Ok(())
    }

    #[allow(unused)]
    pub fn reset() -> Result<()> {
        let config = Config::default();
        config.save()?;
        Ok(())
    }
}

impl ToString for Config {
    fn to_string(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }
}
