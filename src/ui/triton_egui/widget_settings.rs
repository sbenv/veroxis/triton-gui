use bevy_egui::egui::{Color32, RichText};
use bevy_egui::{egui, EguiContexts};

use super::ACCENT_COLOR;
use crate::config::{Config, TritonGuiConfig};
use crate::gitlab::triton_api::TritonApi;
use crate::ui::triton_egui::UiState;

pub fn render(
    ui_widget_state: &mut UiState,
    egui_context: &mut EguiContexts,
    triton_api: &mut TritonApi,
    triton_gui_config: &TritonGuiConfig,
) {
    if ui_widget_state.display_settings_menu {
        egui::Window::new("Settings".to_string())
            .open(&mut ui_widget_state.display_settings_menu)
            .resizable(true)
            .show(egui_context.ctx_mut(), |ui| {
                ui.label("");

                ui.horizontal(|ui| {
                    ui.label("Config Path:");
                    ui.label(
                        RichText::new(Config::get_config_path().to_string_lossy())
                            .color(ACCENT_COLOR),
                    );
                });

                ui.horizontal(|ui| {
                    // date picker min_date
                    ui.horizontal(|ui| {
                        ui.label("Earliest Date");
                        ui.push_id("date_picker_min_date", |ui| {
                            let date_picker = egui_extras::DatePickerButton::new(
                                &mut ui_widget_state.selected_min_date,
                            );
                            if ui.add(date_picker).changed() {
                                triton_api.set_min_date(ui_widget_state.selected_min_date);
                            }
                        });
                    });
                    // date picker max_date
                    ui.horizontal(|ui| {
                        ui.label("Latest Date");
                        ui.push_id("date_picker_max_date", |ui| {
                            let date_picker = egui_extras::DatePickerButton::new(
                                &mut ui_widget_state.selected_max_date,
                            );
                            if ui.add(date_picker).changed() {
                                triton_api.set_max_date(ui_widget_state.selected_max_date);
                            }
                        });
                    });
                });

                ui.label("");
                ui.label(RichText::new("Connections").heading());
                ui.separator();

                let instances_amount = triton_gui_config.active.triton_instances.iter().len();
                for (idx, connection) in
                    triton_gui_config.active.triton_instances.iter().enumerate()
                {
                    ui.horizontal(|ui| {
                        ui.label("Url:");
                        ui.label(RichText::new(connection.url.as_str()).color(ACCENT_COLOR));
                        let triton_url = triton_api.triton_url();
                        if let Some(triton_url) = triton_url {
                            if triton_url.as_str() == connection.url.as_str() {
                                ui.label(RichText::new("active").color(Color32::LIGHT_RED));
                            }
                        }
                    });
                    ui.horizontal(|ui| {
                        ui.label("Token:");
                        match connection.api_token {
                            Some(_) => ui.label(RichText::new("******").color(ACCENT_COLOR)),
                            None => ui.label(RichText::new("null").color(Color32::DARK_GRAY)),
                        };
                    });
                    ui.horizontal(|ui| {
                        if ui.button("connect").clicked() {
                            if triton_api.is_connected() {
                                triton_api.cleanup();
                            }
                            triton_api.connect(
                                connection.url.clone(),
                                connection.api_token.clone(),
                                ui_widget_state.selected_min_date,
                                ui_widget_state.selected_max_date,
                            );
                        }
                        if ui.button("disconnect").clicked() {
                            triton_api.cleanup();
                        }
                    });
                    if idx < (instances_amount - 1) {
                        ui.separator();
                    }
                }
            });
    }
}
