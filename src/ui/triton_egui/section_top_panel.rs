use bevy_egui::egui::{Color32, RichText};
use bevy_egui::{egui, EguiContexts};

use super::ACCENT_COLOR;
use crate::gitlab::gitlab_store::GitlabStore;
use crate::gitlab::triton_api::TritonApi;

pub fn render(egui_context: &mut EguiContexts, triton_api: &TritonApi, gitlab_store: &GitlabStore) {
    egui::TopBottomPanel::top("top_panel").show(egui_context.ctx_mut(), |ui| {
        egui::menu::bar(ui, |ui| {
            // ui.menu_button("Widgets", |ui| {
            //     ui.checkbox(&mut ui_widget_state.display_settings_menu, "Settings");
            //     ui.separator();
            //     ui.checkbox(
            //         &mut ui_widget_state.display_event_counter,
            //         "WebhookEventCounter",
            //     );
            // });
            match triton_api.is_connected() {
                true => {
                    ui.label("Status:");
                    ui.label(RichText::new("connected").color(Color32::GREEN));
                    ui.label("Connection:");
                    ui.label(
                        RichText::new(
                            triton_api
                                .triton_url()
                                .unwrap_or_else(|| String::from("null")),
                        )
                        .color(ACCENT_COLOR),
                    );
                    ui.label("GitlabStore Size:");
                    ui.label(
                        RichText::new(gitlab_store.received_data.len().to_string())
                            .color(ACCENT_COLOR),
                    );
                    ui.label("Earliest Date:");
                    ui.label(
                        RichText::new(triton_api.triton_min_date().to_string()).color(ACCENT_COLOR),
                    );
                    ui.label("Latest Date:");
                    ui.label(
                        RichText::new(triton_api.triton_max_date().to_string()).color(ACCENT_COLOR),
                    );
                }
                false => {
                    ui.label("Status:");
                    ui.label(RichText::new("offline").color(Color32::RED));
                }
            };
        });
    });
}
