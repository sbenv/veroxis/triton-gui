use bevy_egui::egui::{Color32, RichText};
use bevy_egui::{egui, EguiContexts};

use super::{UiState, ACCENT_COLOR};
use crate::gitlab::gitlab_store::GitlabStore;

pub fn render(
    ui_widget_state: &mut UiState,
    egui_context: &mut EguiContexts,
    gitlab_store: &GitlabStore,
) {
    if ui_widget_state.display_event_counter {
        egui::Window::new("Event Counter".to_string())
            .open(&mut ui_widget_state.display_event_counter)
            .show(egui_context.ctx_mut(), |ui| {
                ui.horizontal(|ui| {
                    ui.label("Total:");
                    if !gitlab_store.received_data.is_empty() {
                        ui.label(
                            RichText::new(gitlab_store.received_data.len().to_string())
                                .color(ACCENT_COLOR),
                        );
                    } else {
                        ui.label(RichText::new("0").color(Color32::GRAY));
                    }
                });
                ui.separator();
                let fields = vec![
                    ("Builds:", gitlab_store.builds.len()),
                    ("Deployments:", gitlab_store.deployments.len()),
                    ("FeatureFlags:", gitlab_store.feature_flags.len()),
                    ("Issues:", gitlab_store.issues.len()),
                    ("MergeRequests:", gitlab_store.merge_requests.len()),
                    ("Notes:", gitlab_store.notes.len()),
                    ("Pipelines:", gitlab_store.pipelines.len()),
                    ("Pushs:", gitlab_store.pushs.len()),
                    ("Releases:", gitlab_store.releases.len()),
                    ("WikiPages:", gitlab_store.wiki_pages.len()),
                ];
                for (key, value) in fields.iter() {
                    ui.horizontal(|ui| {
                        ui.label(*key);
                        if *value > 0 {
                            ui.label(RichText::new(value.to_string()).color(ACCENT_COLOR));
                        } else {
                            ui.label(RichText::new("0").color(Color32::GRAY));
                        }
                    });
                }
            });
    }
}
