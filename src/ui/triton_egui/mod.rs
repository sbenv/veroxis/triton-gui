mod section_central_panel;
mod section_side_panel;
mod section_top_panel;
mod widget_settings;
mod widget_webhook_event_counter;

use bevy::prelude::*;
use bevy_egui::egui::Color32;
use bevy_egui::{egui, EguiContexts, EguiPlugin};
use chrono::{Days, Local, NaiveDate};

use self::section_central_panel::pipelines::PipelineSectionState;
use crate::aggregates::pipelines::PipelineAggregate;
use crate::config::TritonGuiConfig;
use crate::gitlab::gitlab_store::GitlabStore;
use crate::gitlab::triton_api::TritonApi;
use crate::ui::triton_egui::section_central_panel::event_log::EventLogSectionState;

const ACCENT_COLOR: Color32 = Color32::from_rgb(76, 212, 255);
// const ACCENT_COLOR: Color32 = Color32::GREEN;

/// register the `egui` based ui plugin and systems
pub fn register(app: &mut App) {
    app.add_plugins(EguiPlugin);
    app.add_systems(Startup, initialize_egui);
    app.add_systems(Startup, startup_system_create_state);
    app.add_systems(Update, autologin_system);
    app.add_systems(Update, render_ui_system);
}

#[derive(Debug, Clone, Resource)]
pub struct UiState {
    autologin_finished: bool,
    selected_min_date: NaiveDate,
    selected_max_date: NaiveDate,
    display_event_counter: bool,
    display_settings_menu: bool,
    selected_scene: TritonScene,
    pipeline_section_state: PipelineSectionState,
    event_log_section_state: EventLogSectionState,
}

impl Default for UiState {
    fn default() -> Self {
        Self {
            autologin_finished: false,
            selected_min_date: Local::now()
                .naive_utc()
                .date()
                .checked_sub_days(Days::new(1))
                .unwrap(),
            selected_max_date: Local::now().naive_utc().date(),
            display_event_counter: false,
            display_settings_menu: false,
            selected_scene: TritonScene::default(),
            pipeline_section_state: PipelineSectionState::default(),
            event_log_section_state: EventLogSectionState::default(),
        }
    }
}

impl UiState {
    pub fn selected_min_date(&self) -> NaiveDate {
        self.selected_min_date
    }

    pub fn selected_max_date(&self) -> NaiveDate {
        self.selected_max_date
    }
}

#[derive(Debug, Clone, Copy, Resource, Default, Eq, PartialEq)]
pub enum TritonScene {
    #[default]
    Pipelines,
    Projects,
    Runners,
    WebhookLog,
    Settings,
}

pub fn startup_system_create_state(mut commands: Commands) {
    commands.insert_resource(UiState::default());
}

pub fn autologin_system(
    mut ui_widget_state: ResMut<UiState>,
    triton_gui_config: Res<TritonGuiConfig>,
    mut triton_api: ResMut<TritonApi>,
) {
    if ui_widget_state.autologin_finished {
        return;
    }
    ui_widget_state.autologin_finished = true;
    if let Some(default_instance) = triton_gui_config.active.triton_instance_default.as_ref() {
        for instance_config in triton_gui_config.active.triton_instances.iter() {
            if &instance_config.url == default_instance {
                info!("autologin");
                triton_api.connect(
                    instance_config.url.clone(),
                    instance_config.api_token.clone(),
                    ui_widget_state.selected_min_date(),
                    ui_widget_state.selected_max_date(),
                );
                break;
            }
        }
    }
}

pub fn render_ui_system(
    mut ui_widget_state: ResMut<UiState>,
    mut egui_context: EguiContexts,
    mut triton_api: ResMut<TritonApi>,
    triton_gui_config: Res<TritonGuiConfig>,
    gitlab_store: Res<GitlabStore>,
    pipeline_aggregate: Res<PipelineAggregate>,
) {
    section_top_panel::render(&mut egui_context, &triton_api, &gitlab_store);
    section_side_panel::render(&mut ui_widget_state, &mut egui_context);
    section_central_panel::render(
        &mut ui_widget_state,
        &mut egui_context,
        &gitlab_store,
        &pipeline_aggregate,
    );
    widget_webhook_event_counter::render(&mut ui_widget_state, &mut egui_context, &gitlab_store);
    widget_settings::render(
        &mut ui_widget_state,
        &mut egui_context,
        &mut triton_api,
        &triton_gui_config,
    );
}

pub fn initialize_egui(mut egui_context: EguiContexts) {
    let ctx = egui_context.ctx_mut();
    let mut visuals = egui::Visuals::dark();
    visuals.hyperlink_color = Color32::from_rgb(190, 190, 190);

    // visuals.extreme_bg_color = Color32::from_rgb(0x10, 0x10, 0x10);
    // visuals.override_text_color = Some(Color32::from_rgb(255, 255, 255));
    // visuals.panel_fill = Color32::from_rgb(0x15, 0x16, 0x17);
    // visuals.widgets.active.bg_fill = Color32::from_rgb(0x3d, 0x85, 0xe0);
    // visuals.widgets.inactive.bg_fill = Color32::from_rgb(0x27, 0x49, 0x72);
    // visuals.widgets.hovered.bg_fill = Color32::from_rgb(66, 150, 250);

    ctx.set_visuals(visuals);
}
