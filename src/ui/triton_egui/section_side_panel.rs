use bevy_egui::{egui, EguiContexts};

use super::{TritonScene, UiState};

pub fn render(ui_widget_state: &mut UiState, egui_context: &mut EguiContexts) {
    egui::SidePanel::left("left_menu")
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.vertical(|ui| {
                egui::Frame::none()
                    .outer_margin(egui::emath::Vec2 { x: 15.0, y: 10.0 })
                    .show(ui, |ui| {
                        if ui
                            .radio(
                                ui_widget_state.selected_scene == TritonScene::Projects,
                                "Projects",
                            )
                            .clicked()
                        {
                            ui_widget_state.selected_scene = TritonScene::Projects;
                        }
                        if ui
                            .radio(
                                ui_widget_state.selected_scene == TritonScene::Pipelines,
                                "Pipelines",
                            )
                            .clicked()
                        {
                            ui_widget_state.selected_scene = TritonScene::Pipelines;
                        }
                        if ui
                            .radio(
                                ui_widget_state.selected_scene == TritonScene::Runners,
                                "Runners",
                            )
                            .clicked()
                        {
                            ui_widget_state.selected_scene = TritonScene::Runners;
                        }
                        if ui
                            .radio(
                                ui_widget_state.selected_scene == TritonScene::WebhookLog,
                                "WebhookLog",
                            )
                            .clicked()
                        {
                            ui_widget_state.selected_scene = TritonScene::WebhookLog;
                        }
                        if ui
                            .radio(
                                ui_widget_state.selected_scene == TritonScene::Settings,
                                "Settings",
                            )
                            .clicked()
                        {
                            ui_widget_state.selected_scene = TritonScene::Settings;
                        }
                    });
                egui::Frame::none()
                    .outer_margin(egui::emath::Vec2 { x: 5.0, y: 10.0 })
                    .show(ui, |ui| {
                        ui.heading("Widgets");
                    });
                egui::Frame::none()
                    .outer_margin(egui::emath::Vec2 { x: 15.0, y: 0.0 })
                    .show(ui, |ui| {
                        ui.checkbox(
                            &mut ui_widget_state.display_event_counter,
                            "WebhookEventCounter",
                        );
                        ui.checkbox(&mut ui_widget_state.display_settings_menu, "Settings");
                    });
            });
        });
}
