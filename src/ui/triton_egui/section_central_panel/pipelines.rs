use std::collections::BTreeMap;

use bevy::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::{Color32, RichText, Ui};
use triton_gitlab::parse_triton_recv::ParseTritonRecv;
use triton_gitlab::raw::events::build::BuildStep;
use triton_gitlab::raw::events::pipeline::PipelineStep;

use crate::aggregates::pipelines::{BuildId, PipelineAggregate};
use crate::ui::triton_egui::ACCENT_COLOR;

const COLOR_STEP_CREATED: Color32 = Color32::DARK_GRAY;
const COLOR_STEP_WAITING_FOR_RESOURCE: Color32 = Color32::DARK_GRAY;
const COLOR_STEP_PENDING: Color32 = Color32::from_rgb(155, 155, 0);
const COLOR_STEP_PREPARING: Color32 = COLOR_STEP_PENDING;
const COLOR_STEP_RUNNING: Color32 = Color32::from_rgb(0x17, 0x93, 0xD0);
const COLOR_STEP_SUCCESS: Color32 = Color32::from_rgb(0x24, 0x66, 0x3B);
const COLOR_STEP_CANCELED: Color32 = Color32::DARK_GRAY;
const COLOR_STEP_FAILED: Color32 = Color32::from_rgb(0xEC, 0x59, 0x41);
const COLOR_STEP_SKIPPED: Color32 = Color32::DARK_GRAY;
const COLOR_STEP_UNKNOWN: Color32 = Color32::WHITE;

const PIPELINE_RENDER_LIMIT_DEFAULT: usize = 100;

#[derive(Debug, Clone, Resource)]
pub struct PipelineSectionState {
    pub filter_matching_project_name: String,
    pub filter_not_matching_project_name: String,
    pub filter_hide_pipeline_succeeded: bool,
    pub filter_hide_pipeline_failed: bool,
    pub filter_hide_pipeline_running: bool,
    pub filter_hide_pipeline_preparing: bool,
    pub filter_hide_pipeline_skipped: bool,
    pub filter_hide_pipeline_canceled: bool,
    pub pipeline_render_limit: Option<usize>,
}

impl Default for PipelineSectionState {
    fn default() -> Self {
        Self {
            filter_matching_project_name: String::new(),
            filter_not_matching_project_name: String::new(),
            filter_hide_pipeline_succeeded: false,
            filter_hide_pipeline_failed: false,
            filter_hide_pipeline_running: false,
            filter_hide_pipeline_preparing: false,
            filter_hide_pipeline_skipped: true,
            filter_hide_pipeline_canceled: true,
            pipeline_render_limit: Some(PIPELINE_RENDER_LIMIT_DEFAULT),
        }
    }
}

pub fn render(
    pipeline_section_state: &mut PipelineSectionState,
    ui: &mut Ui,
    pipeline_aggregate: &PipelineAggregate,
) {
    egui::ScrollArea::vertical()
        .auto_shrink([false, false])
        .show(ui, |ui| {
            let active_pipelines =
                pipeline_aggregate.pipelines_running + pipeline_aggregate.pipelines_preparing;

            egui::Frame::none()
                .outer_margin(egui::emath::Vec2 { x: 5.0, y: 5.0 })
                .show(ui, |ui| {
                    ui.heading(format!("Pipelines - {active_pipelines} active"));
                });
            egui::CollapsingHeader::new("Stats")
                .default_open(false)
                .show(ui, |ui| {
                    ui.horizontal(|ui| {
                        render_pipeline_stats(ui, pipeline_aggregate);
                        render_build_stats(ui, pipeline_aggregate);
                    });
                });
            egui::CollapsingHeader::new("Filters")
                .default_open(false)
                .show(ui, |ui| {
                    ui.horizontal(|ui| {
                        ui.label("Must match Project Name");
                        ui.text_edit_singleline(
                            &mut pipeline_section_state.filter_matching_project_name,
                        );
                        if ui.button("clear").clicked() {
                            pipeline_section_state.filter_matching_project_name.clear();
                        }
                    });
                    ui.horizontal(|ui| {
                        ui.label("May not match Project Name");
                        ui.text_edit_singleline(
                            &mut pipeline_section_state.filter_not_matching_project_name,
                        );
                        if ui.button("clear").clicked() {
                            pipeline_section_state
                                .filter_not_matching_project_name
                                .clear();
                        }
                    });
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_succeeded,
                        "Hide Pipelines with state \"success\"",
                    );
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_failed,
                        "Hide Pipelines with state \"failed\"",
                    );
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_running,
                        "Hide Pipelines with state \"running\"",
                    );
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_preparing,
                        "Hide Pipelines with state \"preparing\"",
                    );
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_skipped,
                        "Hide Pipelines with state \"skipped\"",
                    );
                    ui.checkbox(
                        &mut pipeline_section_state.filter_hide_pipeline_canceled,
                        "Hide Pipelines with state \"canceled\"",
                    );
                    ui.horizontal(|ui| {
                        let mut render_limit_enabled =
                            pipeline_section_state.pipeline_render_limit.is_some();
                        if ui
                            .checkbox(&mut render_limit_enabled, "Enable Pipeline render limit")
                            .clicked()
                        {
                            match render_limit_enabled {
                                true => {
                                    pipeline_section_state.pipeline_render_limit =
                                        Some(PIPELINE_RENDER_LIMIT_DEFAULT)
                                }
                                false => pipeline_section_state.pipeline_render_limit = None,
                            };
                        }
                        if let Some(render_limit) =
                            pipeline_section_state.pipeline_render_limit.as_mut()
                        {
                            ui.add(egui::Slider::new(render_limit, 0..=1000));
                        }
                    });
                });
            ui.separator();
            let mut rendered_pipelines_count = 0;
            for (_, pipeline_event_collection) in pipeline_aggregate.pipelines.iter().rev() {
                if let Some(pipeline_event) = pipeline_event_collection.pipeline_events.last() {
                    match &pipeline_event.object_attributes.status {
                        PipelineStep::Created => {
                            if pipeline_section_state.filter_hide_pipeline_preparing {
                                continue;
                            }
                        }
                        PipelineStep::WaitingForResource => {
                            if pipeline_section_state.filter_hide_pipeline_preparing {
                                continue;
                            }
                        }
                        PipelineStep::Pending => {
                            if pipeline_section_state.filter_hide_pipeline_preparing {
                                continue;
                            }
                        }
                        PipelineStep::Running => {
                            if pipeline_section_state.filter_hide_pipeline_running {
                                continue;
                            }
                        }
                        PipelineStep::Success => {
                            if pipeline_section_state.filter_hide_pipeline_succeeded {
                                continue;
                            }
                        }
                        PipelineStep::Canceled => {
                            if pipeline_section_state.filter_hide_pipeline_canceled {
                                continue;
                            }
                        }
                        PipelineStep::Failed => {
                            if pipeline_section_state.filter_hide_pipeline_failed {
                                continue;
                            }
                        }
                        PipelineStep::Skipped => {
                            if pipeline_section_state.filter_hide_pipeline_skipped {
                                continue;
                            }
                        }
                        PipelineStep::Unknown(_) => {}
                    };
                    if !pipeline_event
                        .project
                        .path_with_namespace
                        .contains(pipeline_section_state.filter_matching_project_name.as_str())
                    {
                        continue;
                    }
                    if !pipeline_section_state
                        .filter_not_matching_project_name
                        .is_empty()
                        && pipeline_event.project.path_with_namespace.contains(
                            pipeline_section_state
                                .filter_not_matching_project_name
                                .as_str(),
                        )
                    {
                        continue;
                    }
                    if let Some(render_limit) = pipeline_section_state.pipeline_render_limit {
                        if rendered_pipelines_count >= render_limit {
                            break;
                        }
                    }
                    egui::Frame::none()
                        .fill(egui::Color32::from_black_alpha(235))
                        .inner_margin(10.0)
                        .outer_margin(5.0)
                        .rounding(5.0)
                        .shadow(egui::epaint::Shadow {
                            extrusion: 1.0,
                            color: Color32::from_white_alpha(20),
                        })
                        .show(ui, |ui| {
                            render_pipeline(ui, pipeline_event, pipeline_event_collection);
                        });
                    rendered_pipelines_count += 1;
                }
            }
        });
}

fn color_by_pipeline_step(step: &PipelineStep) -> Color32 {
    use triton_gitlab::raw::events::pipeline::PipelineStep::*;
    match step {
        Created => COLOR_STEP_CREATED,
        WaitingForResource => COLOR_STEP_WAITING_FOR_RESOURCE,
        Pending => COLOR_STEP_PENDING,
        Running => COLOR_STEP_RUNNING,
        Success => COLOR_STEP_SUCCESS,
        Canceled => COLOR_STEP_CANCELED,
        Failed => COLOR_STEP_FAILED,
        Skipped => COLOR_STEP_SKIPPED,
        Unknown(_) => COLOR_STEP_UNKNOWN,
    }
}

fn color_by_build_step(step: &BuildStep) -> Color32 {
    use triton_gitlab::raw::events::build::BuildStep::*;
    match step {
        Created => COLOR_STEP_CREATED,
        WaitingForResource => COLOR_STEP_WAITING_FOR_RESOURCE,
        Pending => COLOR_STEP_PENDING,
        Running => COLOR_STEP_RUNNING,
        Success => COLOR_STEP_SUCCESS,
        Canceled => COLOR_STEP_CANCELED,
        Failed => COLOR_STEP_FAILED,
        Unknown(_) => COLOR_STEP_UNKNOWN,
    }
}

fn render_pipeline(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
    pipeline_aggregate: &crate::aggregates::pipelines::Pipeline,
) {
    render_pipeline_title(ui, last_pipeline_event);
    ui.separator();
    ui.horizontal(|ui| {
        ui.vertical(|ui| {
            ui.label("");
        });
        ui.vertical(|ui| {
            render_pipeline_info(ui, last_pipeline_event, pipeline_aggregate);
            render_builds(ui, last_pipeline_event, &pipeline_aggregate.builds);
        });
    });
}

fn render_pipeline_title(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
) {
    ui.horizontal_wrapped(|ui| {
        ui.horizontal(|ui| {
            let time = last_pipeline_event
                .parse_triton_recv()
                .map(|d| d.format("%Y-%m-%d %H:%M:%S").to_string())
                .unwrap_or_else(|| String::from("null"));
            ui.label(time);
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Status");
            ui.colored_label(
                color_by_pipeline_step(&last_pipeline_event.object_attributes.status),
                last_pipeline_event.object_attributes.status.to_string(),
            );
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Project");
            ui.colored_label(
                ACCENT_COLOR,
                last_pipeline_event.project.path_with_namespace.clone(),
            );
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Source");
            ui.colored_label(
                ACCENT_COLOR,
                last_pipeline_event.object_attributes.source.clone(),
            );
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Branch");
            ui.colored_label(
                ACCENT_COLOR,
                last_pipeline_event.object_attributes.r#ref.clone(),
            );
        });
        ui.separator();
        ui.horizontal(|ui| {
            ui.label("Link");
            let link_suffix = format!("/-/pipelines/{}", last_pipeline_event.object_attributes.id);
            if ui.link(link_suffix.as_str()).clicked() {
                let pipeline_url = format!("{}{link_suffix}", last_pipeline_event.project.web_url);
                if let Err(error) = open::that(pipeline_url.as_str()) {
                    error!("failed to open url: {error}");
                }
            }
        });
    });
}

fn render_pipeline_stats(ui: &mut Ui, pipeline_aggregate: &PipelineAggregate) {
    ui.vertical(|ui| {
        ui.horizontal(|ui| {
            ui.label("Pipelines Total:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_total.to_string()).color(ACCENT_COLOR),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Succeeded:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_succeeded.to_string())
                    .color(COLOR_STEP_SUCCESS),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Failed:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_failed.to_string())
                    .color(COLOR_STEP_FAILED),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Running:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_running.to_string())
                    .color(COLOR_STEP_RUNNING),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Preparing:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_preparing.to_string())
                    .color(COLOR_STEP_PREPARING),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Skipped:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_skipped.to_string())
                    .color(COLOR_STEP_SKIPPED),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Pipelines Canceled:");
            ui.label(
                RichText::new(pipeline_aggregate.pipelines_canceled.to_string())
                    .color(COLOR_STEP_CANCELED),
            );
        });
    });
}

fn render_pipeline_info(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
    _pipeline_aggregate: &crate::aggregates::pipelines::Pipeline,
) {
    ui.horizontal_wrapped(|ui| {
        egui::Frame::none()
            .fill(egui::Color32::from_black_alpha(215))
            .inner_margin(10.0)
            .outer_margin(egui::emath::Vec2 { x: 0.0, y: 5.0 })
            .rounding(1.0)
            .shadow(egui::epaint::Shadow {
                extrusion: 1.0,
                color: Color32::from_white_alpha(20),
            })
            .show(ui, |ui| {
                ui.monospace(last_pipeline_event.commit.message.trim());
            });
    });
}

fn render_builds(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
    builds: &BTreeMap<BuildId, Vec<triton_gitlab::raw::events::build::Build>>,
) {
    ui.push_id(
        format!(
            "pipeline_builds_dropdown_{}",
            last_pipeline_event.object_attributes.id
        ),
        |ui| {
            egui::CollapsingHeader::new(format!("Builds ({})", builds.len()))
                .default_open(false)
                .show(ui, |ui| {
                    render_build_info(ui, last_pipeline_event, builds);
                });
            render_build_progress(ui, last_pipeline_event, builds);
        },
    );
}

fn render_build_info(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
    build_events: &BTreeMap<BuildId, Vec<triton_gitlab::raw::events::build::Build>>,
) {
    egui::Frame::none()
        .inner_margin(egui::emath::Vec2 { x: 0.0, y: 8.0 })
        .show(ui, |ui| {
            egui::Grid::new(format!(
                "pipeline_builds_table_{}",
                last_pipeline_event.object_attributes.id
            ))
            .num_columns(6)
            .spacing((20.0, 5.0))
            .show(ui, |ui| {
                ui.label("First Event Received");
                ui.label("Last Event Received");
                ui.label("Status");
                ui.label("Build");
                ui.label("Runner");
                ui.label("Link");
                ui.end_row();
                for (_build_id, builds) in build_events.iter() {
                    if let Some(first_build) = builds.first() {
                        if let Some(build) = builds.last() {
                            let time = first_build
                                .parse_triton_recv()
                                .map(|d| d.format("%Y-%m-%d %H:%M:%S").to_string())
                                .unwrap_or_else(|| String::from("null"));
                            ui.label(time);
                            let time = build
                                .parse_triton_recv()
                                .map(|d| d.format("%Y-%m-%d %H:%M:%S").to_string())
                                .unwrap_or_else(|| String::from("null"));
                            ui.label(time);
                            ui.colored_label(
                                color_by_build_step(&build.build_status),
                                build.build_status.to_string(),
                            );
                            ui.colored_label(ACCENT_COLOR, build.build_name.as_str());
                            let runner_name: String = match &build.runner {
                                Some(runner) => runner.description.to_owned(),
                                None => "null".to_string(),
                            };
                            ui.colored_label(ACCENT_COLOR, runner_name.as_str());
                            let link_suffix = format!("/-/jobs/{}", build.build_id);
                            if ui.link(link_suffix.as_str()).clicked() {
                                let pipeline_url =
                                    format!("{}{link_suffix}", last_pipeline_event.project.web_url);
                                if let Err(error) = open::that(pipeline_url.as_str()) {
                                    error!("failed to open url: {error}");
                                }
                            }
                            ui.end_row();
                        }
                    }
                }
            });
        });
}

fn render_build_stats(ui: &mut Ui, pipeline_aggregate: &PipelineAggregate) {
    ui.vertical(|ui| {
        ui.horizontal(|ui| {
            ui.label("Builds Total:");
            ui.colored_label(ACCENT_COLOR, pipeline_aggregate.builds_total.to_string());
        });
        ui.horizontal(|ui| {
            ui.label("Builds Success:");
            ui.colored_label(
                COLOR_STEP_SUCCESS,
                pipeline_aggregate.builds_success.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Failed:");
            ui.colored_label(
                COLOR_STEP_FAILED,
                pipeline_aggregate.builds_failed.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Canceled:");
            ui.colored_label(
                COLOR_STEP_CANCELED,
                pipeline_aggregate.builds_canceled.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Running:");
            ui.colored_label(
                COLOR_STEP_RUNNING,
                pipeline_aggregate.builds_running.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Pending:");
            ui.colored_label(
                COLOR_STEP_PENDING,
                pipeline_aggregate.builds_pending.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Waiting for Resource:");
            ui.colored_label(
                COLOR_STEP_WAITING_FOR_RESOURCE,
                pipeline_aggregate.builds_waiting_for_resource.to_string(),
            );
        });
        ui.horizontal(|ui| {
            ui.label("Builds Created:");
            ui.colored_label(
                COLOR_STEP_CREATED,
                pipeline_aggregate.builds_created.to_string(),
            );
        });
    });
}

fn render_build_progress(
    ui: &mut Ui,
    last_pipeline_event: &triton_gitlab::raw::events::pipeline::Pipeline,
    builds: &BTreeMap<BuildId, Vec<triton_gitlab::raw::events::build::Build>>,
) {
    let mut completed_builds = 0.0_f32;
    let mut reached_builds = 0.0_f32;
    let mut running_builds = 0.0_f32;
    let mut builds_could_run = 0.0_f32;
    for (_, build_event_list) in builds.iter() {
        if let Some(build) = build_event_list.last() {
            use BuildStep::*;
            match build.build_status {
                Created | WaitingForResource | Pending | Running | Failed | Unknown(_) => {}
                Canceled | Success => completed_builds += 1.0,
            }
            match build.build_status {
                Created => {}
                WaitingForResource | Pending | Running | Canceled | Success | Failed
                | Unknown(_) => reached_builds += 1.0,
            }
            match build.build_status {
                WaitingForResource | Pending | Created | Canceled | Success | Failed
                | Unknown(_) => {}
                Running => running_builds += 1.0,
            }
            match build.build_status {
                Pending | Running => builds_could_run += 1.0,
                WaitingForResource | Created | Canceled | Success | Failed | Unknown(_) => {}
            }
        }
    }
    use PipelineStep::*;
    let should_render_progress_bar = match last_pipeline_event.object_attributes.status {
        Created | WaitingForResource | Pending | Running => true,
        Success | Canceled | Failed | Skipped | Unknown(_) => false,
    };
    let builds_count = builds.len() as f32;
    let percent_succeeded = completed_builds / builds_count;
    let percent_reached = reached_builds / builds_count;
    let percent_running = running_builds / builds_could_run;
    if should_render_progress_bar && percent_succeeded < 1.0 {
        egui::Grid::new(format!(
            "build_progress_{}",
            last_pipeline_event.object_attributes.id
        ))
        .num_columns(3)
        .max_col_width(640.0_f32)
        .show(ui, |ui| {
            ui.label("Builds Reached".to_string());
            ui.label(format!("{reached_builds} of {builds_count}"));
            ui.add(egui::ProgressBar::new(percent_reached).show_percentage());
            ui.end_row();
            ui.label("Builds Running".to_string());
            ui.label(format!("{running_builds} of {builds_could_run}"));
            ui.add(egui::ProgressBar::new(percent_running).show_percentage());
            ui.end_row();
            ui.label("Builds Finished".to_string());
            ui.label(format!("{completed_builds} of {builds_count}"));
            ui.add(egui::ProgressBar::new(percent_succeeded).show_percentage());
            ui.end_row();
        });
    }
}
