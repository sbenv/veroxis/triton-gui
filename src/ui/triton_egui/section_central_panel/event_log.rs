use bevy::prelude::*;
use bevy_egui::egui;
use bevy_egui::egui::{TextEdit, Ui};

use crate::gitlab::gitlab_store::GitlabStore;

#[derive(Debug, Clone, Resource)]
pub struct EventLogSectionState {
    event_log_filter: String,
    event_log_line_limit: u64,
}

impl Default for EventLogSectionState {
    fn default() -> Self {
        Self {
            event_log_filter: String::default(),
            event_log_line_limit: 50,
        }
    }
}

pub fn render(
    event_log_section_state: &mut EventLogSectionState,
    ui: &mut Ui,
    gitlab_store: &GitlabStore,
) {
    egui::ScrollArea::vertical()
        .auto_shrink([false, false])
        .show(ui, |ui| {
            ui.horizontal(|ui| {
                ui.label("Filter: ");
                ui.text_edit_singleline(&mut event_log_section_state.event_log_filter);
                ui.label("Limit: ");
                ui.add(egui::Slider::new(
                    &mut event_log_section_state.event_log_line_limit,
                    0..=2000,
                ));
            });
            ui.separator();
            if event_log_section_state.event_log_line_limit > 0 {
                let mut counter = 0;
                for (idx, webhook_event) in gitlab_store.received_data.iter().enumerate().rev() {
                    if let Some(object_kind) = webhook_event["object_kind"].as_str() {
                        if let Some(triton_recv) = webhook_event["triton_recv"].as_str() {
                            if !event_log_section_state.event_log_filter.is_empty()
                                && !object_kind
                                    .contains(event_log_section_state.event_log_filter.as_str())
                            {
                                continue;
                            }
                            if let Ok(mut json) = serde_json::to_string_pretty(webhook_event) {
                                egui::CollapsingHeader::new(format!(
                                    "[{idx}] {object_kind} {triton_recv}"
                                ))
                                .default_open(false)
                                .show(ui, |ui| {
                                    ui.add(
                                        TextEdit::multiline(&mut json)
                                            .desired_rows(1)
                                            .desired_width(f32::INFINITY),
                                    );
                                });
                                ui.separator();
                                counter += 1;
                                if counter >= event_log_section_state.event_log_line_limit {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        });
}
