pub mod event_log;
pub mod pipelines;
pub mod settings;

use bevy_egui::{egui, EguiContexts};

use super::{TritonScene, UiState};
use crate::aggregates::pipelines::PipelineAggregate;
use crate::gitlab::gitlab_store::GitlabStore;

pub fn render(
    ui_widget_state: &mut UiState,
    egui_context: &mut EguiContexts,
    gitlab_store: &GitlabStore,
    pipeline_aggregate: &PipelineAggregate,
) {
    egui::CentralPanel::default().show(egui_context.ctx_mut(), |ui| {
        match ui_widget_state.selected_scene {
            TritonScene::WebhookLog => {
                event_log::render(
                    &mut ui_widget_state.event_log_section_state,
                    ui,
                    gitlab_store,
                );
            }
            TritonScene::Pipelines => {
                pipelines::render(
                    &mut ui_widget_state.pipeline_section_state,
                    ui,
                    pipeline_aggregate,
                );
            }
            TritonScene::Settings => {}
            TritonScene::Projects => {}
            TritonScene::Runners => {}
        }
    });
}
