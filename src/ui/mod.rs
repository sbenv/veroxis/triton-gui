pub mod triton_egui;

use bevy::prelude::*;

/// register all ui plugins and systems
pub fn register(app: &mut App) {
    triton_egui::register(app);
}
