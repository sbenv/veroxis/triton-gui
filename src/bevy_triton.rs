use bevy::log::{Level, LogPlugin};
use bevy::prelude::*;

/// register selected core bevy plugins
pub fn register(app: &mut App) {
    app.add_plugins(DefaultPlugins.set(log_plugin()).set(window_plugin()));
}

fn log_plugin() -> LogPlugin {
    #[cfg(debug_assertions)]
    let log_plugin = LogPlugin {
        level: Level::DEBUG,
        filter: String::from("triton_gui=debug,error"),
    };

    #[cfg(not(debug_assertions))]
    let log_plugin = LogPlugin {
        level: Level::INFO,
        filter: String::from("triton_gui=info,error"),
    };

    log_plugin
}

fn window_plugin() -> WindowPlugin {
    WindowPlugin {
        // window: WindowDescriptor {
        //     title: "Triton Desktop".to_string(),
        //     // present_mode: PresentMode::AutoNoVsync,
        //     present_mode: PresentMode::AutoVsync,
        //     position: WindowPosition::Centered,
        //     ..default()
        // },
        ..default()
    }
}
