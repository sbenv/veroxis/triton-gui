mod aggregates;
mod bevy_triton;
mod config;
mod gitlab;
mod ui;

use bevy::prelude::*;

pub fn start_app() {
    let mut app = App::new();
    aggregates::register(&mut app);
    bevy_triton::register(&mut app);
    config::register(&mut app);
    gitlab::register(&mut app);
    ui::register(&mut app);
    app.run();
}
